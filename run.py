import imaputil

from collections import defaultdict, deque, Counter
import datetime
import email
import functools
import json
import nltk
import numpy as np
import matplotlib.pyplot as plt
import os.path
import pandas as pd
import pickle
import pprint
import re
import redis
import string
import time
import logging

plt.style.use('ggplot')

logger = logging.getLogger(__name__)
ch = logging.StreamHandler()
formatter = logging.Formatter('[%(levelname)s]\t[%(filename)s:%(funcName)s:%(lineno)d]\t%(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
logger.setLevel(logging.DEBUG)

redis_client = redis.StrictRedis(host='localhost', port=6379, db=0)

CANONICAL_EMAILS = {
        'daypay@umich.edu'      : 'david.r.pedersen@gmail.com',
        'cwolff@umich.edu'      : 'wolffeee13@gmail.com',
        'crichar8@umd.edu'      : 'clint.a.richardson@gmail.com',
        'sacapiloa@gmail.com'   : 'dan.fhg@gmail.com',
        'sigal.boris@gmail.com' : 'bagelmsu@gmail.com'}

IGNORE_EMAILS = [
        'recoilingwalrus@googlegroups.com',
        'noreply@googlegroups.com',
        'smaatta@stanford.edu']

EMAILS = {
        'archisj@gmail.com'               : 'AJ',
        'bagelmsu@gmail.com'              : 'BS',
        'clint.a.richardson@gmail.com'    : 'CR',
        'sperberdan@gmail.com'            : 'DS',
        'dan.fhg@gmail.com'               : 'DH',
        'daniel.hernandezgardiol@epfl.ch' : 'DH',
        'david.r.pedersen@gmail.com'      : 'DP',
        'max.l.meyers@gmail.com'          : 'MM',
        'noah.m.israel@gmail.com'         : 'NI',
        'owen.campbell@gmail.com'         : 'OC',
        'rosen21@gmail.com'               : 'ER',
        'wolffeee13@gmail.com'            : 'CW'}

# http://jmlr.org/papers/volume5/lewis04a/a11-smart-stop-list/english.stop
STOP_WORDS = open('english_stopwords.txt', 'r').read().splitlines()

NAMES = {
        'archis' : 'AJ',
        'boris'  : 'BS',
        'bor'    : 'BS',
        'chris'  : 'CW',
        'wolffee': 'CW',
        'clint'  : 'CR',
        'dafe'   : 'DH',
        'sperb'  : 'DS',
        'dave'   : 'DP',
        'david'  : 'DP',
        'evan'   : 'ER',
        'ev'     : 'ER',
        'max'    : 'MM',
        'noah'   : 'NI',
        'owen'   : 'OC'}

IDS = list(set(NAMES.values()))

def redis_cache(redis_client, cache_args=[]):
  def redis_cache_(fn):
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
      keyparts = [('fn', fn.__name__)]

      argnames = fn.func_code.co_varnames
      for cache_arg in cache_args:
        arg_index = argnames.index(cache_arg)
        if arg_index < len(args):
          val = str(args[arg_index])
        elif cache_arg in kwargs:
          val = str(kwargs[cache_arg])
        keyparts.append((cache_arg, val))
      key = ','.join('='.join(item) for item in keyparts)
      lookup = redis_client.get(key)
      if lookup:
        return pickle.loads(lookup)
      else:
        rval = fn(*args, **kwargs)
        redis_client.set(key, pickle.dumps(rval))
        return rval
    return wrapper
  return redis_cache_

@redis_cache(redis_client)
def get_uids(conn):
  typ, data = conn.uid('search', None, r'(X-GM-RAW "subject:\"Recoiling Walrus\" newer:2015/01/01")')
  uid_str = data[0]
  uids = uid_str.split()
  logger.info('found %d uids', len(uids))
  return uids

@redis_cache(redis_client, ['uids'])
def get_messages(conn, uids):
  messages = {}
  print 'downloading {0} messages'.format(len(uids))
  for i, uid in enumerate(uids):
    if uid not in messages:
      messages[uid] = get_message(conn, uid)
    if (i % 10) == 0:
      print 'downloaded {} messages'.format(i)
      #redis_client.bgsave()
      #print 'saved to dump.rdb'
  return messages

@redis_cache(redis_client, ['uid'])
def get_message(conn, uid):
  typ, msg_data = conn.uid('fetch', uid, '(RFC822)') # get everything
  #typ, msg_data = conn.uid('fetch', uid, '(BODY[HEADER.FIELDS (SUBJECT FROM DATE)])')
  for response_part in msg_data:
    if isinstance(response_part, tuple):
      msg = email.message_from_string(response_part[1])
      return msg

def parse_date(msg):
  date_str = msg['date']
  date_tuple = email.utils.parsedate(date_str)
  timestamp = time.mktime(date_tuple)
  return datetime.datetime.fromtimestamp(timestamp).isoformat()

def parse_sender(header):
  m = re.match('(.*\s?<)?([^>]+)>?', header)
  if m:
    sender = m.group(2).lower()
  else:
    logger.warning('could not parse email from header: %s', header)
    sender = None
  return sender

def parse_body(msg):
  raw = ''
  for part in msg.walk():
    if part.get_content_type() == 'text/plain':
      raw += part.get_payload()
  new_lines = []
  for line in raw.splitlines():
    m1 = re.match('^\s*[<>]+.*', line)
    m2 = re.match('^\s*\-*\s*[Oo]riginal [Mm]es.*', line)
    m3 = re.match('^\s*[Oo]n.*201[01234].*', line)
    m4 = re.match('^\S{76}', line)
    m5 = re.match('^\s*Quoting.*', line)
    m6 = re.match('^\s*-{5}.*', line)
    m7 = re.match('.*wrote:', line)
    if not any(map(bool, [m1])):
      if not any(map(bool, [m2, m3, m4, m5, m6, m7])): # these signal end of good part
        new_lines.append(line)
      else:
        break
  final = '\n'.join(new_lines)
  if len(final) > 10000:
    logger.debug('suspicious message: %s' % final)
    return ''
  else:
    return final

def clean_messages(messages):
  for message in messages:
    sender = parse_sender(message['from'])
    sender = CANONICAL_EMAILS.get(sender, sender)
    if sender not in IGNORE_EMAILS:
      message['from'] = sender
      message['id'] = EMAILS[sender]
      yield message

def tokenize(body):
  # split using mediocre off-the-shelf nltk tokenizer
  tokens = nltk.word_tokenize(body)
  clean_tokens = []
  for tok in tokens:
    tok = str.lower(tok)
    if (tok not in STOP_WORDS):
      if not any([p in tok for p in string.punctuation]):
        clean_tokens.append(tok)
  return clean_tokens

def make_author_model(ids):
  author_counts = sorted(Counter(ids).items(), key=lambda (a,c) : c)
  authors, counts = zip(*author_counts)

  # counts
  plt.clf()
  xticks = range(len(counts))
  plt.bar(xticks, counts, align='center')
  ax = plt.gca()
  ax.set_xticks(xticks)
  ax.set_xticklabels(authors)
  plt.title('Number of Emails Sent by Author')
  plt.xlabel('Author')
  plt.ylabel('Number of Emails Sent')
  plt.savefig('author_counts.pdf')

  # normalized
  total_count = sum(counts)
  frac = [float(count) / total_count for count in counts]
  plt.clf()
  plt.bar(xticks, frac, align='center')
  ax = plt.gca()
  ax.set_xticks(xticks)
  ax.set_xticklabels(authors)
  plt.title('Fraction of Emails Sent by Author')
  plt.xlabel('Author')
  plt.ylabel('Fraction of Emails Sent')
  plt.savefig('author_dist.pdf')

def make_markov_model(seq, reverse=False, order=1):
  """ written to be compatible with generators """
  if reverse:
    seq = iter(reversed(seq))
  else:
    seq = iter(seq)
  trans = defaultdict(Counter)
  context = deque()
  for i in range(order):
    context.append(seq.next())
  for e in seq:
    trans[e][tuple(context)] += 1
    context.append(e)
    context.popleft()
  for e, counts in trans.items():
    dir_str = 'reverse' if reverse else 'forward'
    fname ='{0}_order_{1}_markov_{2}_contexts.txt'.format(dir_str, order, e)
    with open(fname, 'w') as f:
      for (context, count) in counts.most_common(10):
        f.write(str(context) + ', ' + str(count) + '\n')
  return trans

def make_response_model(seq):
  responses = pd.DataFrame(index=IDS, columns=IDS)
  responses = responses.fillna(0)
  prev = seq.next()
  for curr in seq:
    responses[curr][prev] += 1
    prev = curr
  make_heatmap(responses, 'Column Responds to Row', 'responses.pdf')
  norm_responses = responses / responses.sum()
  make_heatmap(norm_responses, 'Column Responds to Row (Normalized by Column)',
      'norm_responses.pdf')

def make_frequency_graphs(dates, senders):
  df = pd.DataFrame({'senders' : senders, 'date' : pd.to_datetime(dates),
      'value' : [1]*len(senders)})
  df = df.set_index(pd.DatetimeIndex(df['date']))
  periods = [('D', 'days'), ('W', 'weeks'), ('M', 'months'), ('Q', 'quarters')]
  for (period, period_name) in periods:
    df = df.resample(period, how='sum')
    df = df.fillna(0)
    df.plot()
    plt.title('Number of Emails Sent (by {0})'.format(period_name))
    plt.xlabel('Date ({0})'.format(period_name))
    plt.ylabel('Number of Emails Sent')
    plt.savefig('%s_timeseries.pdf' % (period.lower()))

def make_modulo_graphs(dates, senders):
  df = pd.DataFrame({'senders' : senders, 'date' : pd.to_datetime(dates),
      'value' : [1]*len(senders)})
  df = df.set_index(pd.DatetimeIndex(df['date']))
  group_funs =  [
          ('time_of_day', 'Hour of Day', lambda d : d.hour),
          ('day_of_week', 'Day of Week', lambda d : d.dayofweek)]
  plt.clf()
  for type, name, group_fun in group_funs:
    df_mod = df.copy()
    df_mod = df_mod.groupby(group_fun).sum()
    df_mod = df_mod.fillna(0)
    df_mod.plot()
    plt.title('Number of Emails Sent (by {0})'.format(name))
    plt.xlabel('Date ({0})'.format(name))
    plt.ylabel('Number of Emails Sent')
    plt.savefig('%s.pdf' % (type))

def make_language_model(bodies):
  all_tokens = reduce(list.__iadd__, bodies, [])
  counts = Counter(all_tokens)
  # write out only words which will be displayed on word cloud
  topk = zip(*counts.most_common(400))[0]
  top_tokens = filter(lambda w: w in topk, all_tokens)
  f = open('top_tokens.txt', 'w')
  f.write('\n'.join(top_tokens))
  f.close()

def make_personal_language_models(ids, bodies, k=20):
  lm = defaultdict(Counter)
  for id, body in zip(ids, bodies):
     lm[id].update(body)
  mentions = pd.DataFrame(index=IDS, columns=IDS)
  for id, cntr in lm.items():
    #logger.info('\n\n\ntop words for %s\n%s', id, pprint.pformat(cntr.most_common(k)))
    for name, mentioned_id in NAMES.items():
      mentions[id][mentioned_id] = float(cntr.get(name, 0))
  logger.info('\ncolumn is the mentioner, row is the mentionee')
  make_heatmap(mentions, 'Column Mentioned Row', 'raw_mentions.pdf')
  norm_mentions = mentions.copy()
  # zero-out self-mentions
  for id1 in IDS:
    for id2 in IDS:
      if id1 == id2:
        norm_mentions[id1][id2] = 0
  norm_mentions = norm_mentions / norm_mentions.sum()
  make_heatmap(norm_mentions, 'Column Mentioned Row (Normalized by Column)',
      'norm_mentions.pdf')

def make_heatmap(df, title, fname):
  logger.info('heatmap for: %s\n%s', title, df)
  plt.clf()
  plt.pcolor(np.float32(df))
  plt.yticks(np.arange(0.5, len(df.index), 1), df.index)
  plt.xticks(np.arange(0.5, len(df.columns), 1), df.columns)
  plt.title(title)
  plt.savefig(fname)
  plt.show()

def main():
  config = json.load(open('client_secrets.json', 'r'))['installed']
  config['scope'] = 'https://mail.google.com/'
  config['user'] = 'rosen21@gmail.com'
  config['token_cache'] = 'token_cache.json'

  conn = imaputil.get_imap_conn(config['user'], imaputil.get_token(config))
  uids = get_uids(conn)
  messages = get_messages(conn, uids)
  conn.close()
  conn.logout()
  messages = sorted(messages.values(), key=lambda msg: parse_date(msg))
  messages = list(clean_messages(messages))

  # markov models
  ids = [msg['id'] for msg in messages]
  make_author_model(iter(ids))
  make_response_model(iter(ids))
  for reverse in (False, True):
    for order in range(1,3):
      make_markov_model(ids, reverse=reverse, order=order)

  # timeseries charts
  dates = [parse_date(msg) for msg in messages]
  make_frequency_graphs(dates, ids)
  make_modulo_graphs(dates, ids)

  # content analysis
  bodies = [tokenize(parse_body(msg)) for msg in messages]
  make_language_model(bodies)
  make_personal_language_models(ids, bodies)

if __name__ == "__main__":
  main()
