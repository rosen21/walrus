import base64
import imaplib
import json
import logging
import os
import time
import urllib
import webbrowser

logger = logging.getLogger(__name__)
ch = logging.StreamHandler()
formatter = logging.Formatter('[%(levelname)s]\t[%(filename)s:%(funcName)s:%(lineno)d]\t%(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
logger.setLevel(logging.INFO)

# google oauth stuff adapted from:
# http://google-mail-oauth2-tools.googlecode.com/svn/trunk/python/oauth2.py

# The URL root for accessing Google Accounts.
GOOGLE_ACCOUNTS_BASE_URL = 'https://accounts.google.com'

# Hardcoded dummy redirect URI for non-web apps.
REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob'

def url_escape(text):
  return urllib.quote(text, safe='~-._')

def format_url_params(params):
  param_fragments = []
  for param in sorted(params.iteritems(), key=lambda x: x[0]):
    param_fragments.append('%s=%s' % (param[0], url_escape(param[1])))
  return '&'.join(param_fragments)

def accounts_url(command):
  return '%s/%s' % (GOOGLE_ACCOUNTS_BASE_URL, command)

def generate_permission_url(client_id, scope):
  params = {}
  params['client_id'] = client_id
  params['redirect_uri'] = REDIRECT_URI
  params['scope'] = scope
  params['response_type'] = 'code'
  return '%s?%s' % (accounts_url('o/oauth2/auth'),
                    format_url_params(params))

def authorize_tokens(client_id, client_secret, authorization_code):
  params = {}
  params['client_id'] = client_id
  params['client_secret'] = client_secret
  params['code'] = authorization_code
  params['redirect_uri'] = REDIRECT_URI
  params['grant_type'] = 'authorization_code'
  request_url = accounts_url('o/oauth2/token')

  print params

  response = urllib.urlopen(request_url, urllib.urlencode(params)).read()
  return json.loads(response)

def generate_oauth2_string(username, access_token, base64_encode=True):
  auth_string = 'user=%s\1auth=Bearer %s\1\1' % (username, access_token)
  if base64_encode:
    auth_string = base64.b64encode(auth_string)
  return auth_string

def get_token(config):
  if os.path.isfile(config['token_cache']):
    token_info = json.load(open(config['token_cache'], 'r'))
    #if time.clock() > token_info['issued'] + token_info['expires_in']:
    if True:
      refreshed_token_info = refresh_token(config['client_id'], config['client_secret'],
              token_info['refresh_token'])
      token_info.update(refreshed_token_info)
      token_info['issued'] = time.clock()
      json.dump(token_info, open(config['token_cache'], 'w'))
  else:
    auth_url = generate_permission_url(config['client_id'], config['scope'])
    webbrowser.open(auth_url)
    logger.info('if a browser window does not automatically pop up, go to: %s',
            auth_url)
    authorization_code = raw_input('Enter verification code: ')
    token_info = authorize_tokens(config['client_id'], config['client_secret'],
                                authorization_code)
    token_info['issued'] = time.clock()
    json.dump(token_info, open(config['token_cache'], 'w'))
  return token_info['access_token']

def refresh_token(client_id, client_secret, refresh_token):
  params = {}
  params['client_id'] = client_id
  params['client_secret'] = client_secret
  params['refresh_token'] = refresh_token
  params['grant_type'] = 'refresh_token'
  request_url = accounts_url('o/oauth2/token')

  response = urllib.urlopen(request_url, urllib.urlencode(params)).read()
  return json.loads(response)

def get_imap_conn(user, access_token):
  auth_string = generate_oauth2_string(user, access_token,
                           base64_encode=False)
  imap_conn = imaplib.IMAP4_SSL('imap.gmail.com')
  imap_conn.debug = 0
  try:
    imap_conn.authenticate('XOAUTH2', lambda x: auth_string)
  except:
    raise Exception('token expired')
  status, all_msgs = imap_conn.select('[Gmail]/All Mail', readonly=True)
  logger.debug('connection status: %s', status)
  return imap_conn
